<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Model {

	private $table = "user";
	private $_data = array();

	public function validate()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$this->db->where("UEmail", $username);
		$query = $this->db->get($this->table);

		if ($query->num_rows()) 
		{
			$row = $query->row_array();

			if ($row['Upassword'] == sha1($password)) 
			{
				unset($row['Upassword']);
				$this->session->set_userdata('id', $row['UId']);
				
				$this->_data = $row;
				return ERR_NONE;
			}

			return ERR_INVALID_PASSWORD;
		}
		else {
			return ERR_INVALID_USERNAME;
		}
	}
	public function validaterRegister(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules("UNombre", "Nombre", "trim|required");
		$this->form_validation->set_rules("UEmail", "email", "trim|required");
		$this->form_validation->set_rules("Upassword", "contraseña", "trim|required");
		$this->form_validation->set_rules("password2", "Confirmar contraseña", "trim|required|matches[Upassword]");	
		return $this->form_validation->run();
	}

	public function registrar(){
		$data = array(
			'UNombre' => $this->input->post('UNombre'),
			'UEmail' => $this->input->post('UEmail'),
			'Upassword' => sha1($this->input->post('Upassword'))
		);

		$query = $this->db->insert($this->table, $data);
		$last_id = $this->db->insert_id();
		$this->session->set_userdata('id',$last_id);
	}

	public function get_data()
	{
		return $this->_data;
	}


	function ListadoUsuarios()
	{
		$this->db->select();
		$this->db->from($this->table);

		$query = $this->db->get();
		return $query->result();
	}
}