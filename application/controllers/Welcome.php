<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		 parent::__construct();
		// $this->foo = $foo;
		$this->load->library('form_validation');
		$this->load->model('login', 'auth');
		$this->load->library('session');
	}

	public function logged_in_check()
	{
		if ($this->session->userdata("logged_in")) {
			redirect("dashboard");
		}
	}

	public function index()
	{	
		$this->logged_in_check();
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules("username", "Username", "trim|required");
		$this->form_validation->set_rules("password", "Password", "trim|required");
		if ($this->form_validation->run() == true) 
		{
			
			$status = $this->auth->validate();
			if ($status == ERR_INVALID_USERNAME) {
				$this->session->set_flashdata("error", "Usuario invalido");
				$this->load->view("login");
			}
			elseif ($status == ERR_INVALID_PASSWORD) {
				$this->session->set_flashdata("error", "Contraseña invalidad");
				$this->load->view("login");
			}
			else
			{
				
				$this->load->view("dashboard");
			}
		}else{
			// $this->load->view("macro/header");		
			$this->load->view("login");
			// $this->load->view("macro/footer");	
		}

		
	}

	public function register(){
		
		$this->load->view('register');
	}

	public function registrar(){
		/*echo "string";
		exit();*/
		if($this->auth->validaterRegister()){
			if ($this->auth->registrar()) {
				redirect("dashboard");
			}else{
				
				$this->load->view('register');
			}
		}else{
			$this->load->view('register');
		}
	}


	public function logout()
	{
		$this->session->unset_userdata("logged_in");
		$this->session->sess_destroy();
		redirect("Welcome");
	}

	public function Lista(){
		$data["usuarios"] = $this->auth->ListadoUsuarios();
		$this->load->view('lista', $data);
	}
}
